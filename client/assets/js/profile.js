let params = new URLSearchParams(window.location.search)

let userId = params.get('userId')

let token = localStorage.getItem("token")

let courseId = localStorage.getItem('._id')

fetch('http://localhost:3000/api/users/details', {
	method: 'GET',
	headers: {
		'Content-Type': 'application/json',
		'Authorization': `Bearer ${token}`
	}
})
.then(res => {
	return res.json()
})
.then(data => {
	// console.log(data)
	data.name = data.firstName + " " + data.lastName

	var enrollmentIds = []
	enrollments = data.enrollments
		for (let i = 0; i < enrollments.length; i++){
			enrollmentIds.push(enrollments[i].courseId)
				}

		fetch('http://localhost:3000/api/courses')
		.then(res => res.json())
		.then(data => {
			console.log(data)
			let courseNames = []
			for(let j = 0; j < data.length; j++){
				if(enrollmentIds.includes(data[j]._id)){
					courseNames.push(data[j].name)
					console.log(courseNames)	
				}
			}

		let enrolledCourse = courseNames.join("<br>")
		
		let program = document.querySelector('#coursesContainer')
				program.innerHTML = `		
				<h5 class="my-5 text-center">Course Enrolled</h5>
					<div class="container text-center">${enrolledCourse}</div>
					`
		
	})
		let container = document.querySelector('#profileContainer')
	container.innerHTML = `<div class="col-md-12">
	<div class="jumbotron card text-center">
  <div class="card-header" style="font-size: 50px;">${data.name}
  </div>
  <div class="card-body">
    <div class="card-center">${data.email}</div>
    <div class="card-center">${data.mobileNo}</div>
    <div id="coursesContainer"></div>
  </div>
</div>
</div>`

})
		