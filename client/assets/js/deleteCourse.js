
let params = new URLSearchParams(window.location.search);
// get method returns the value of the key passed in as an argument

let courseId = params.get('courseId')
let token = localStorage.getItem('token')

fetch(`http://localhost:3000/api/courses/${courseId}`,{
    method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                courseId: courseId,
            })
        })
        .then(res => {
            return res.json()
        })
        .then(data => {
            //creation of new course successful
            if(data === true){
                //redirect to courses index page
                window.location.replace("./courses.html")
            }else{
                //error in creating course, redirect to error page
                alert("Something went wrong")
            }
        })