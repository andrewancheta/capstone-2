//window.location.search returns the query string part of the URL
// console.log(window.location.search)

//instantiate a URLSearchParams object so we can execute methods to access parts of the query string
let params = new URLSearchParams(window.location.search)

let courseId = params.get('courseId')
let adminUser = localStorage.getItem("isAdmin")

//retrieve the JWT stored in our local storage
let token = localStorage.getItem("token");
//retrieve the userId stored in our local storage
let userId = localStorage.getItem("id");

if (adminUser == "false" || !adminUser){
	let courseName = document.querySelector("#courseName");
	let courseDesc = document.querySelector("#courseDesc");
	let coursePrice = document.querySelector("#coursePrice");
	let enrollContainer = document.querySelector("#enrollContainer");

	fetch(`http://localhost:3000/api/courses/${courseId}`)
	.then((res) => res.json())
	.then((data) => {

		courseName.innerHTML = data.name
		courseDesc.innerHTML = data.description
		coursePrice.innerHTML = data.price
		enrollContainer.innerHTML = `<button id="enrollButton" class="btn btn-block btn-primary">Enroll</button>`

		let enrollButton = document.querySelector("#enrollButton")

		enrollButton.addEventListener("click", () => {
			//enroll the user for the course
			fetch('http://localhost:3000/api/users/enroll', {
				method: "PUT",
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${token}`
				},
				body: JSON.stringify({
					courseId: courseId,
					userId: userId
				})
			})
			.then(res => {
				return res.json()
			})
			.then(data => {
				if(data === true){
					//enrollment is successful
					alert("Thank you for enrolling! See you!")
					window.location.replace('./courses.html')
				}else{
					alert("Enrollment failed")
				}
			})
		})
	})
}else{
	
	fetch('http://localhost:3000/api/courses')
	.then(res => res.json())
	.then(data => {
		// console.log(data)
		let cour =[]
		for(let i = 0; i < data.length; i++){
			cour.push(data[i].id)
			// console.log(enrollees)
		}
	
		console.log(cour)
		fetch('http:localhost:3000/api/users')
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			let userName = []
			for (let j = 0; j < data.length; j++){
				if(cour.includes(data[j].id)){
					userName.push(data[j].firstName)
					// console.log(userName)
				}
			}
			let enrolledUser = userName.join("<br>")
			console.log(enrolledUser)
			let student = document.querySelector('#enrolledUser')
			student.innerHTML = `<div id = "viewList" class="row">		
			<div class="col-md-6 offset-md-3">
				<section class="jumbotron">
					<h2 id="courseName" class="mt-5">Enrolled User</h2>
					<p class="lead">${enrolledUser}</p>
					<a href="./courses.html" lass="btn btn-light text-primary btn-block editButton">Back to Courses</a>
				</section>
				
			</div>
		</div>`
		})
		

	})
	
	let container = document.querySelector('#viewList')
	container.innerHTML = `<div class="container my-5 text-center" id = "enrolledUser"></div>`
}